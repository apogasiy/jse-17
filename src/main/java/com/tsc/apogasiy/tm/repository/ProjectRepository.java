package main.java.com.tsc.apogasiy.tm.repository;

import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.enumerated.Status;
import main.java.com.tsc.apogasiy.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void add(final Project project) {
        projects.add(project);
    }

    @Override
    public void remove(final Project project) {
        projects.remove(project);
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projectList = new ArrayList<>(projects);
        projectList.sort(comparator);
        return projectList;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public boolean isEmpty() {
        return projects.isEmpty();
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public boolean existsByIndex(int index) {
        return index < projects.size() && index >= 0;
    }

    @Override
    public boolean existsByName(String name) {
        return findByName(name) != null;
    }

    @Override
    public Project findById(String id) {
        for (Project project : projects)
            if (id.equals(project.getId())) return project;
        return null;
    }

    @Override
    public Project findByName(String name) {
        for (Project project : projects)
            if (name.equals(project.getName())) return project;
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        projects.remove(project);
        return project;

    }

    @Override
    public Project removeByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        projects.remove(project);
        return project;

    }

    @Override
    public Project removeByIndex(int index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        projects.remove(project);
        return project;

    }

    @Override
    public Project startById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishById(String id) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByIndex(Integer index) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishByName(String name) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        final Project project = findById(id);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        final Project project = findByIndex(index);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null)
            return null;
        project.setStatus(status);
        return project;
    }

}
