package main.java.com.tsc.apogasiy.tm.component;

import main.java.com.tsc.apogasiy.tm.api.repository.ICommandRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.IProjectRepository;
import main.java.com.tsc.apogasiy.tm.api.repository.ITaskRepository;
import main.java.com.tsc.apogasiy.tm.api.service.*;
import main.java.com.tsc.apogasiy.tm.command.AbstractCommand;
import main.java.com.tsc.apogasiy.tm.command.project.*;
import main.java.com.tsc.apogasiy.tm.command.system.*;
import main.java.com.tsc.apogasiy.tm.command.task.*;
import main.java.com.tsc.apogasiy.tm.exception.system.UnknownCommandException;
import main.java.com.tsc.apogasiy.tm.repository.CommandRepository;
import main.java.com.tsc.apogasiy.tm.repository.ProjectRepository;
import main.java.com.tsc.apogasiy.tm.repository.TaskRepository;
import main.java.com.tsc.apogasiy.tm.service.*;
import main.java.com.tsc.apogasiy.tm.util.TerminalUtil;

public class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final IProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final ITaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    private final ILogService logService = new LogService();

    {
        registry(new DisplayAllCommand());
        registry(new AboutCommand());
        registry(new ExitCommand());
        registry(new HelpCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());

        registry(new ProjectListShowCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskAddToProjectByIdProjectTaskCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListByProjectIdProjectTaskCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskRemoveFromProjectByIdProjectTaskCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
    }

    public void start(final String... args) {
        displayWelcome();
        runArgs(args);
        logService.debug("Test environment");
        while (true) {
            try {
                System.out.println("ENTER COMMAND");
                final String command = TerminalUtil.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0)
            return false;
        AbstractCommand command = commandService.getCommandByName(args[0]);
        if (command == null)
            throw new UnknownCommandException(args[0]);
        command.execute();
        return true;
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty())
            return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null)
            throw new UnknownCommandException(command);
        abstractCommand.execute();
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IProjectService getProjectService() {
        return projectService;
    }

    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public ICommandService getCommandService() {
        return commandService;
    }

}
